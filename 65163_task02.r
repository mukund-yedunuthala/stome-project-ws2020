################################################################################
# Stochastic Methods for Materials Science
# Programing Project | WS2020-21
# TU Bergakademie Freiberg
################################################################################
# Venkata Mukund Kashyap Yedunuthala
# M. No. 65163
################################################################################
# TASK 02
################################################################################

source("libs.r")
source("fcts.r")
y1 <- readImage("image1_09.png")
display(y1, method="raster")

## taking a set 
A <- y1
B1 <- makeBrush(size=3, shape='disc') # disc of radius 1 pixel
A1 <- opening(A, B1)
writeImage(A1,"./task02/02_a1.png")

B1 <- makeBrush(size=5, shape='disc') # disc of radius 2 pixels
A1 <- opening(A, B1)
writeImage(A1,"./task02/02_a2.png")

B1 <- makeBrush(size=7, shape='disc') # disc of radius 3 pixels
A1 <- opening(A, B1)
writeImage(A1,"./task02/02_a3.png")

B1 <- makeBrush(size=9, shape='disc') # disc of radius 4 pixels
A1 <- opening(A, B1)
writeImage(A1,"./task02/02_a4.png")

# at disc radius 4 pixels most disappear
getAA.rect.const = function(nrep = 39, lambda, a, b, W = owin(c(0,1),c(0,1)), m=0, spacing=1) {
    sapply(1:nrep, function(k) {
        XYABP = rBM.rect.const(lambda=lambda, a=a, b=b, W=W)
        BW.XYABP = digitizeRectSys(XYABP, spacing=spacing) 
        estALXFct(BW=BW.XYABP, m=m, spacing=spacing, ms = TRUE)[,2]
    })
}
AA.model.999.1 = getAA.rect.const(nrep=999, lambda=4.7, a=1.71, b=0.15 , W = owin(c(0,10),c(0,10)), m=m, spacing=0.025) 

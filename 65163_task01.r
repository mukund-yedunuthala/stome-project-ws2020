################################################################################
# Stochastic Methods for Materials Science
# Programing Project | WS2020-21
# TU Bergakademie Freiberg
################################################################################
# Venkata Mukund Kashyap Yedunuthala
# M. No. 65163
################################################################################
# TASK 01
################################################################################

# sourcing local libraries
source('libs.r')
source('fcts.r')
# reading images
y1 <- readImage("./image1_09.png")
y2 <- readImage("./image2_09.png")
display(y1, method = "raster")
display(y2, method = "raster")
# estimating Minkowski functions
# First image
y1.ALX <- estALXFct(BW=y1, m=35)
png(file="./task01/01_01_all.png", width=480, height=480)
plotALXFct(BW=y1, ALX=y1.ALX, show.type=0)
dev.off()
# Second image
y2.ALX <- estALXFct(BW=y2, m=35)
png(file="./task01/01_02_all.png", width=480, height=480)
plotALXFct(BW=y2, ALX=y2.ALX, show.type=0)
dev.off() 
# plotting comparison plots
png(file="./task01/01_cmp_MK1.png",width=480,height=480)
plotALXFct(ALX=y1.ALX,ALX2=y2.ALX,show.type=1)
dev.off()
png(file="./task01/01_cmp_MK2.png",width=480,height=480)
plotALXFct(ALX=y1.ALX,ALX2=y2.ALX,show.type=2)
dev.off()
png(file="./task01/01_cmp_MK3.png",width=480,height=480)
plotALXFct(ALX=y1.ALX,ALX2=y2.ALX,show.type=3)
dev.off()
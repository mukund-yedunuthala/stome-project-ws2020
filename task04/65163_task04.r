source('libs.r')
source('fcts.r')

lambda <- c(5, 10, 15)
R <- 0.05
res <- c()
rM3.getEmp <- function(lambda, R, W = owin(c(0,10), c(0,10)), spacing = 0.01){
    data <- sapply(1:10, function(k){
        XYR.M3 <- rM3.disc.const(lambda = lambda, R = R, W = W)
        BW.XYR.M3 <- digitizeDiscSys(XYR.M3, spacing = spacing)
        estALX(BW.XYR.M3, spacing = spacing)
    })
    empMean = mean(data[1,])
    empSD = sd(data[2,])
    return(c(empMean,empSD))
}
res <- rM3.getEmp(lambda, R = R)

################################################################################
# Stochastic Methods for Materials Science
# Programing Project | WS2020-21
# TU Bergakademie Freiberg
################################################################################
# Venkata Mukund Kashyap Yedunuthala
# M. No. 65163
################################################################################
# TASK 04
################################################################################

# sourcing local files
source('libs.r')
source('fcts.r')

# setting up vector of intensities for underlying Poisson processes
lambda <- c(5, 10, 15)
rM3.getEmp <- function(lambda, R = 0.05, W = owin(c(0,10), c(0,10)), spacing = 0.01){
# generating samples of matern - III model
    data <- sapply(1:100, function(k){
        XYR.M3 <- rM3.disc.const(lambda = lambda, R = R, W = W)
        BW.XYR.M3 <- digitizeDiscSys(XYR.M3, spacing = spacing)
        estALX(BW.XYR.M3, spacing = spacing)
    })
# print out information i.e. empirical mean and empirical standard deviations
    print(toString(lambda))
    means <- c(mean(data[1,]), mean(data[2,]), mean(data[3,]))
    print("Means: ")
    print(means)
    sds <- c(sd(data[1,]), sd(data[2,]), sd(data[3,]))
    print("Standard deviations: ")
    print(sds)
# creating box plots and saving as png
    name <- paste("./task04/boxplot_AA_",toString(lambda), ".png", sep="")
    png(file = name, width = 480, height = 480)
    boxplot(data[1,], main="AA")
    dev.off()
    name <- paste("./task04/boxplot_LA_",toString(lambda), ".png", sep="")
    png(file = name, width = 480, height = 480)
    boxplot(data[2,], main="LA")
    dev.off()
    name <- paste("./task04/boxplot_XA_",toString(lambda), ".png", sep="")
    png(file = name, width = 480, height = 480)
    boxplot(data[3,], main="XA")
    dev.off()
    return(data)    
}
d <- sapply(lambda, rM3.getEmp)

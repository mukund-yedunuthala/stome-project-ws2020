################################################################################
# Stochastic Methods for Materials Science
# Programing Project | WS2020-21
# TU Bergakademie Freiberg
################################################################################
# Venkata Mukund Kashyap Yedunuthala
# M. No. 65163
################################################################################
# TASK 03
################################################################################

# sourcing available functions and libraries
source('libs.r')
source('fcts.r')

# reading image
BW <- readImage("image3_09.png")
display(BW, method = "raster")
BW.ALX <- estALX(BW, spacing = 0.001)

# estimating parameters per task 03a
VS <- c(BW.ALX[1],(4/pi)*BW.ALX[2])

# to extract diameters and centers
BW.labelled <- bwlabel(BW)
BW.feat <- computeFeatures.moment(BW.labelled)

dia.2D <- BW.feat[,3]
# switching unit to mm 
dia.2D <- dia.2D/1000

# disc centers given by
center.2D <- cbind(BW.feat[,1], BW.feat[,2])
# range of diameters of these discs
range.2D <- range(dia.2D)

# applying reduction in window
dia.2D.red <- BW.feat[BW.feat[,1]>30 & BW.feat[,1]<1371 & BW.feat[,2]>30 & BW.feat[,2]<1371, 3]
dia.2D.red <- dia.2D.red/1000
# number of discs with center in reduced window
num.2D.red <- length(dia.2D.red)

# writing histogram of diameters of reduced discs to png
png(file="./task03/03_hist_red_dia.png",width=480,height=480)
Delta <- 0.02
fig.2D = hist(dia.2D.red, breaks=seq(0, 0.12, by=Delta))$counts
dev.off()

# estimating empirical features
emp.Feat = c(mean(dia.2D.red),sd(dia.2D.red))

# area of the window in mm^2
area.2D.win = prod(dim(BW)-60)/1000^2
Ni.2D = fig.2D/area.2D.win

# applying Scheil-Schwartz-Saltykov method
Ni.3D = saltykov(Delta = Delta, Ni.2D)

# writing distribution of diameters to png
png(file="./task03/03_barplot_frq.png",width=600,height=600)
barplot(rbind(Ni.2D/sum(Ni.2D),Ni.3D/sum(Ni.3D)), beside=TRUE, legend.text = c("2D", "3D"), args.legend = list(x = "topleft"),names.arg=c("(0,0.02]","(0.02,0.04]","(0.04,0.06]","(0.06,0.08]","(0.08,0.10]","(0.10,0.12]"))
dev.off()

# estimating intensities and mean ball diameters
estLM.1 = c(sum(1/dia.2D.red)*2/pi/area.2D.win, num.2D.red*pi/2/sum(1/dia.2D.red))
estLM.2 = c(sum(Ni.3D), num.2D.red/(sum(Ni.3D) * area.2D.win))

# printing out final information
print("(a) The volume fraction and specific surface area (under the assumption of isotropy) of the underlying 3D structure is: ")
VS
print("(b) The range of diameters after implementation of segmentation algorithm: ")
range.2D
print("(b) The number of discs after application of minus sampling: ")
num.2D.red
print("(b) The empirical mean and empirical standard deviation of these diameters is: ")
emp.Feat
print("(d) The estimates for mean number of balls per unit volume and the mean ball diameter are: ")
estLM.2

